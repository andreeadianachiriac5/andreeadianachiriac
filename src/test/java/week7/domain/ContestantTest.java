package week7.domain;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assertions;

class ContestantTest {


    @Test
    @DisplayName("convert_minutes_to_seconds_without_penalty_success()")
    void convert_minutes_to_seconds_without_penalty_success(){
        assertEquals(Contestant.converterToMinutes(new String[]{"28","30"}, 0), 1710);
    }

    @Test
    @DisplayName("convert_minutes_to_seconds_with_penalty_success()")
    void convert_minutes_to_seconds_with_penalty_success(){
        assertEquals(Contestant.converterToMinutes(new String[]{"28","30"}, 15), 1725);
    }

    @Test
    @DisplayName("is_finalTime_normalTime_plus_penalty_fail")
    void is_finalTime_normalTime_plus_penalty_fail(){
        System.out.println("It needs another conversion (from seconds to hour) in order to become final time.");
        Contestant contestant = new Contestant(new String[]{"11", "Umar Jorgson","SK","30:27", "xxxox","xxxxx","xxoxo"});

        contestant.setPenalty("10");
        contestant.setTime("30:15");
        contestant.setFinalTime("30:25");
        assertFalse(contestant.getFinalTime().equals(contestant.getTime()+contestant.getPenalty()));
    }

    @Test
    @DisplayName("converter_from_seconds_to_minutes")
    void converter_from_seconds_to_minutes(){
        assertTrue(Contestant.converterToFinalScore(1750).equals("29:10"));
    }

    @Test
    @DisplayName("is_name_correct")
    void is_name_correct(){
        Contestant contestant = new Contestant(new String[]{"11", "Umar Jorgson","SK","30:27", "xxxox","xxxxx","xxoxo"});

        assertTrue(contestant.getName().equals("Umar Jorgson"));
    }




}