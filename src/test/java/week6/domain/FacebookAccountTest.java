package week6.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
//@RunWith(JUnitPlatform.class)
class FacebookAccountTest {

    private FacebookAccount facebookAccount;

    @Mock
    private FacebookAccount facebookAccountSecond;


    @BeforeEach
    void setUp() {
        facebookAccount = new FacebookAccount("Andreea-Diana", 64, new FriendsLimit());
    }


    @Test
    void addFriends_toCurrentNumberOfFriends_success() {
        boolean success = facebookAccount.addFriends(10);
        assertTrue(success);
        assertEquals(74, facebookAccount.getNumberOfFriends());
    }

    @Test
    void removeFriends_fromCurrentNumberOfFriends_success() {
        boolean success = facebookAccount.removeFriends(5);
        assertTrue(success);
        assertEquals(59, facebookAccount.getNumberOfFriends());
    }

    @Test
    void deleteAllFriends() {
        boolean success = facebookAccount.deleteAllFriends();
        assertTrue(success);
        assertEquals(0, facebookAccount.getNumberOfFriends());
    }



    @Test
    void settingANumberOfFriends_addingSomeFriends_success(){
        facebookAccount.setNumberOfFriends(20);
        boolean success = facebookAccount.addFriends(7);
        assertTrue(success);
        assertEquals(27, facebookAccount.getNumberOfFriends());
    }



    @Test
    void howManyFriendsCanYouAdd_limitIs500(){
        FriendsLimit friendsLimit1 = new FriendsLimit();
        FriendsLimit friendsLimit = Mockito.mock(FriendsLimit.class);
        Mockito.when(friendsLimit.getFriendsLimit()).thenReturn(500);
        facebookAccount.setNumberOfFriends(200);

        int friendsWhichCanBeAdd = friendsLimit.getFriendsLimit() - facebookAccount.getNumberOfFriends();

        assertEquals(300, friendsWhichCanBeAdd);

    }
}