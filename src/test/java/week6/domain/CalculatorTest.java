package week6.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {


    @Test
    void calculateTheSameUnit_equals_sameUnit() {
        assertEquals("275.0 mm", Calculator.calculate("100 mm + 150 mm + 25 mm =", "mm"));
    }

    @Test
    void calculateTheSameUnit_equals_desiredUnit(){
        assertEquals("0.07 m", Calculator.calculate("16 cm + 1 cm - 10 cm =", "m"));
    }

    @Test
    void calculateDifferentUnits_equals_desiredUnit(){
        assertEquals("1.09 m", Calculator.calculate("10 cm + 1 m - 10 mm =", "m"));
    }
}