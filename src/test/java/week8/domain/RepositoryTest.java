package week8.domain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest {

    private final static Logger LOGGER = Logger.getLogger(RepositoryTest.class.getName());
    private Repository testRepository = new Repository();

    @Test
    @DisplayName("Size of the catalog after adding 2 students")
    void addStudent() {
        LOGGER.info("Adding students with success.");
        testRepository.addStudent(new Student("Aida", "Economu", "18476", "female", "1994-09-21"));
        testRepository.addStudent(new Student("Rebeca", "Sirbu", "87219", "female", "1994-02-19"));
        assertEquals(testRepository.size(),2);

    }

    @Test
    @DisplayName("Deleting the student by ID.")
    void deleteStudent() {
        LOGGER.info("Deleting one student by ID with success.");
        testRepository.addStudent(new Student("Aida", "Economu", "18476", "female", "1994-09-21"));
        testRepository.addStudent(new Student("Rebeca", "Sirbu", "87219", "female", "1994-02-19"));
        testRepository.deleteStudent("18476");
        assertEquals(testRepository.size(), 1);

    }

    @Test
    @DisplayName("Retrieve students on age")
    void retrieveStudentsWithAge() {
        LOGGER.info("Retrieving two students with success based on age 23 and displaying their name.");
        testRepository.addStudent(new Student("Andreea", "Chiriac", "12345", "female", "1999-07-05"));
        testRepository.addStudent(new Student("Aida", "Economu", "18476", "female", "1994-09-21"));
        testRepository.addStudent(new Student("Calin", "Costea", "14375", "male", "1997-03-25"));
        testRepository.addStudent(new Student("Emanuel", "Sirbu", "67329", "male", "1999-08-16"));
        testRepository.addStudent(new Student("Rebeca", "Sirbu", "87219", "female", "1994-02-19"));
        assertTrue(testRepository.retrieveStudentsWithAge(23));

    }

    @Test
    @DisplayName("Order students by name")
    void orderStudentsByName() {
        LOGGER.info("Sorting the value for the 2 criterion (name). The Criterion can be written from keyboard in Main)");
        testRepository.addStudent(new Student("Andreea", "Chiriac", "12345", "female", "1999-07-05"));
        testRepository.addStudent(new Student("Aida", "Economu", "18476", "female", "1994-09-21"));
        testRepository.addStudent(new Student("Calin", "Costea", "14375", "male", "1997-03-25"));
        testRepository.addStudent(new Student("Emanuel", "Sirbu", "67329", "male", "1999-08-16"));
        testRepository.addStudent(new Student("Rebeca", "Sirbu", "87219", "female", "1994-02-19"));
        assertTrue(testRepository.orderStudentsBy("2"));
    }

    @Test
    @DisplayName("Order students by age")
    void orderStudentsByAge() {
        LOGGER.info("Sorting the value for the 1 criterion (age). The criterion can be written from keyboard in Main.)");
        testRepository.addStudent(new Student("Andreea", "Chiriac", "12345", "female", "1999-07-05"));
        testRepository.addStudent(new Student("Aida", "Economu", "18476", "female", "1994-09-21"));
        testRepository.addStudent(new Student("Calin", "Costea", "14375", "male", "1997-03-25"));
        testRepository.addStudent(new Student("Emanuel", "Sirbu", "67329", "male", "1999-08-16"));
        testRepository.addStudent(new Student("Rebeca", "Sirbu", "87219", "female", "1994-02-19"));
        assertTrue(testRepository.orderStudentsBy("1"));
    }







}