package week8.domain;
import week8.comparators.BirthDateComparator;
import week8.comparators.NameComparator;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Logger;


public final class Repository {
    private final static Logger LOGGER = Logger.getLogger(Repository.class.getName());

    private Set<Student> catalog = new HashSet<>();

    public void showStudents() {
        for (Student student: catalog) System.out.println(student);
    }

    public void addStudent(Student student) {
        if (student.getValidation()) catalog.add(student);
    }

    public void deleteStudent(String inputID) {
        try {
            if (!inputID.isEmpty()) {
                boolean present = true;
                for (Student student : catalog) {
                    if (student.getID().equals(inputID)) {
                        catalog.remove(student);
                        present = false;
                    }
                }
                if (present) throw new IllegalArgumentException("Your input is wrong.");
            }else throw new IllegalArgumentException("Your input is wrong.");
        }catch (Exception exception){
            if (inputID.isEmpty()) LOGGER.warning("Empty String. Please give an ID.");
            else LOGGER.warning("This ID does not exist in our system.");
        }
    }


    public boolean retrieveStudentsWithAge(int age){
        Set<Student> retrievedStudents = new HashSet<>();
        try{
            if (age > 0) {
                for (Student student : catalog) {
                    if (LocalDate.now().getYear() - student.getActualBirthDate().getYear() == age)
                        retrievedStudents.add(student);
                }
                if (!retrievedStudents.isEmpty()) {
                    for (Student student : retrievedStudents) System.out.println(student);
                    return true;
                }
            }else {
                throw new IllegalArgumentException("Invalid input.");
            }
        }catch (Exception e){
            if (age <= 0) LOGGER.warning("Age cannot be negative.");
            if (retrievedStudents.isEmpty()) LOGGER.warning("We do not have any student of this age in system.");
        }
        return false;
    }




    public boolean orderStudentsBy(String choice) {
        try{
            if (choice.isEmpty()){
                throw new IllegalArgumentException("Your input is empty.");
            }else if (choice.equals("1")) {
                Set<Student> orderByBirthdate = new TreeSet<>(new BirthDateComparator());
                for (Student student : catalog) orderByBirthdate.add(student);
                for (Student student: orderByBirthdate) System.out.println(student);
            } else if (choice.equals("2")){

                Set<Student> orderByName = new TreeSet<>(new NameComparator());
                for (Student student : catalog) orderByName.add(student);
                for (Student student : orderByName) System.out.println(student);
            }else throw new IllegalArgumentException("Invalid input");
        }catch (Exception e){
            if (!choice.equals("1") || !choice.equals("2")) LOGGER.warning("Input is not correct. See again the list above!");
            orderStudentsBy(new Scanner(System.in).nextLine());
        }


        return true;
    }

    public int size(){
        return catalog.size();
    }

}




