package week8.domain;
import java.time.LocalDate;
import java.util.logging.Logger;

public final class Student {

    private final static Logger LOGGER = Logger.getLogger(Student.class.getName());
    private String firstName, lastName, gender, ID, birthDate;
    private LocalDate actualBirthDate;
    private boolean validate = true;

    public Student(String firstName, String lastName, String ID, String gender, String birthDate) {
        try{
            if (!firstName.isEmpty() && !lastName.isEmpty()) {
                this.firstName = firstName;
                this.lastName  = lastName;
            }else {
                this.validate = false;
                throw new IllegalArgumentException("The student should have both a first name and a last name. Please check again the complete name!");
            }
        } catch (Exception e){

            if (firstName.isEmpty()) LOGGER.warning("For ID " + ID + " -> First name is empty");
            if (lastName.isEmpty()) LOGGER.warning("For ID " + ID + " -> Last name is empty");
            else LOGGER.warning("For ID " + ID + " -> Both name are missing");
        }

        try {
            if (!birthDate.isEmpty()){
                this.actualBirthDate = LocalDate.parse(birthDate);
                if (actualBirthDate.isAfter(LocalDate.of(1900, 1, 1)) && actualBirthDate.isBefore(LocalDate.of(2002, 1, 1))) {
                    this.birthDate = birthDate;
                }
                else {
                    this.validate = false;
                }
            }else {
                this.validate = false;
                throw new IllegalArgumentException("Your input in invalid.");
            }
        }catch (Exception e){
            if (birthDate.isEmpty()) LOGGER.warning("For ID " + ID + " -> input is empty");
            else LOGGER.warning("For ID " + ID + " -> He may be too old/ young");
        }

        this.ID = ID;

        try{
            if (gender.toUpperCase().equals("MALE") || gender.toUpperCase().equals("FEMALE")){
                this.gender = gender;
            }else {
                this.validate = false;
                throw new IllegalArgumentException("Your input is invalid");
            }

        }catch (Exception e){

            if (gender.isEmpty()) LOGGER.warning("For ID " + ID + " -> input is empty");
            else LOGGER.warning("For ID " + ID + " -> gender is invalid.");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getActualBirthDate() {
        return actualBirthDate;
    }

    public void setActualBirthDate(LocalDate actualBirthDate) {
        this.actualBirthDate = actualBirthDate;
    }

    public boolean getValidation(){
        return validate;
    }
    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", ID='" + ID + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", actualBirthDate=" + actualBirthDate +
                '}';
    }
}
