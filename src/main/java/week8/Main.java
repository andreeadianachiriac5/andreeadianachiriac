package week8;

import week8.domain.Repository;
import week8.domain.Student;
import java.util.Scanner;
import java.util.logging.Logger;


public class Main {

    private static Logger logger = Logger.getLogger("my.class.fqn");

    public static void main(String[] args) {

        /*In main I will give some examples of Students which won't be added in the catalogue. Also, I will display how
        the sorting with an input from keyboard actually works. If the sorting method does not exist, I have entered a
        recursive method which calls again the function and gives you the chance to enter the proper sorting method.*/

        Repository repository = new Repository();
        repository.addStudent(new Student("Andreea", "Chiriac", "12345", "female", "1999-07-05"));
        repository.addStudent(new Student("Mihaela", "", "78921", "female", "2006-04-08"));
        repository.addStudent(new Student("Rebeca", "Sirbu", "87219", "female", "1994-02-19"));
        repository.addStudent(new Student("Corina", "Tabacu", "78921", "unisex", "2006-04-08"));
        repository.addStudent(new Student("Luca", "Popescu", "78321", "male", ""));
        repository.addStudent(new Student("Calin", "Costea", "14375", "male", "1997-03-25"));
        repository.showStudents();


        System.out.println("----------");

        System.out.println("Chose 1 for Birthdate sort.");
        System.out.println("Chose 2 for LastName sort.");
        System.out.println("Enter the value here:");

        Scanner scanner = new Scanner(System.in);
        String choice = scanner.nextLine();

        repository.orderStudentsBy(choice);
    }
}
