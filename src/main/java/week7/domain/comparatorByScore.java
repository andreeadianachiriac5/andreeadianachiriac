package week7.domain;

import week5.domain.Animal;

import java.util.Comparator;

public class comparatorByScore implements Comparator<Contestant> {
    @Override
    public int compare(Contestant o1, Contestant o2) {
        return Integer.compare(o1.getFinalScore(), o2.getFinalScore());
    }
}
