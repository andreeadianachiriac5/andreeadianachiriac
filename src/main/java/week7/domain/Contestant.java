package week7.domain;

public final class Contestant{

    private String number;
    private String name;
    private String country;
    private String time;
    private String firstShot;
    private String secondShot;
    private String thirdShot;
    private String totalShot;
    private int finalScore;
    private String finalTime;
    private String penalty;

    public Contestant(String[] line) {
        this.number = line[0];
        this.name = line[1];
        this.country = line[2];
        this.time = line[3];
        this.firstShot = line[4];
        this.secondShot = line[5];
        this.thirdShot = line[6];
        this.totalShot = firstShot + secondShot + thirdShot;

    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFirstShot() {
        return firstShot;
    }

    public void setFirstShot(String firstShot) {
        this.firstShot = firstShot;
    }

    public String getSecondShot() {
        return secondShot;
    }

    public void setSecondShot(String secondShot) {
        this.secondShot = secondShot;
    }

    public String getThirdShot() {
        return thirdShot;
    }

    public void setThirdShot(String thirdShot) {
        this.thirdShot = thirdShot;
    }

    public String getTotalShot() {
        return totalShot;
    }

    public void setTotalShot(String totalShot) {
        this.totalShot = totalShot;
    }



    public void setFinalScore(int finalScore){
        this.finalScore = finalScore;
    }

    public int getFinalScore(){
        return finalScore;
    }

    public void setFinalTime(String finalTime){
        this.finalTime = finalTime;
    }

    public String getFinalTime(){
        return finalTime;
    }

    public void setPenalty(String penalty){
        this.penalty = penalty;
    }

    public String getPenalty(){
        return this.penalty;
    }

    public static int converterToMinutes(String[] time, int penalty){
        return Integer.parseInt(time[0]) * 60 + Integer.parseInt(time[1]) + penalty;
    }

    public static String converterToFinalScore(int score){
        return Integer.toString(score/60) + ":" + Integer.toString(score%60);
    }

}
