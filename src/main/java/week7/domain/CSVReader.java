package week7.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CSVReader {
    public static final String delimiter = ",";
    public static List<Contestant> contestantsReadAndShow(String csvFile){
        List<Contestant> contestants = new ArrayList<>();
        try {
            File file = new File(csvFile);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = " ";
            String[] tempArr;
            while ((line = br.readLine()) != null){
                tempArr = line.split(delimiter);
                contestants.add(new Contestant(tempArr));
            }
            br.close();
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
        return contestants;
    }
}
