package week7;

import week7.domain.CSVReader;
import week7.domain.Contestant;
import week7.domain.comparatorByScore;

import java.util.*;


public class Main {
    public enum Prize {
        GOLD,
        SILVER,
        BRONZE,
        PARTICIPANT
    }


    public static void main(String[] args){
        String csvFile = "C:\\Users\\Bebe-8\\Desktop\\andreeadianachiriac\\src\\main\\java\\week7\\domain\\skiResults";
        HashMap<String, Integer> scoreBoard = new HashMap<>();
        List<Contestant> contestants = new ArrayList<>();
        contestants = CSVReader.contestantsReadAndShow(csvFile);
        for (Contestant contestant: contestants){
            int penalty = 0;
            for (char shot : contestant.getTotalShot().toCharArray()){
                if (shot == 'o') penalty += 10;
            }
            contestant.setPenalty(Integer.toString(penalty));
            String[] time = contestant.getTime().split(":");
            int score = Contestant.converterToMinutes(time, penalty);
            contestant.setFinalScore(score);
            String finalTime = Contestant.converterToFinalScore(score);
            contestant.setFinalTime(finalTime);
        }


        Set<Contestant> results = new TreeSet<>(new comparatorByScore());

        for (Contestant contestant : contestants){
            results.add(contestant);
        }

        int place = 1;
        for (Contestant contestant : results){
            switch (place) {
                case 1:
                    System.out.println(Prize.GOLD + ": " + contestant.getName() + " -> " + "Initial time: " + contestant.getTime() + "  | Penalty : " + contestant.getPenalty() + " " + "| Final time " + contestant.getFinalTime());
                    break;

                case 2 :
                    System.out.println(Prize.SILVER + ": " + contestant.getName() + " -> " + "Initial time: " + contestant.getTime() + "  | Penalty : " + contestant.getPenalty() + " " + "| Final time " + contestant.getFinalTime());
                    break;

                case 3 :
                    System.out.println(Prize.BRONZE + ": " + contestant.getName() + " -> " + "Initial time: " + contestant.getTime() + "  | Penalty : " + contestant.getPenalty() + " " + "| Final time " + contestant.getFinalTime());
                    break;

                default:
                    System.out.println(Prize.PARTICIPANT + ": " + contestant.getName() + " -> " + "Initial time: " + contestant.getTime() + "  | Penalty : " + contestant.getPenalty() + " " + "| Final time " + contestant.getFinalTime());
            }
            if (place == 3) System.out.println("-----------------------------------------------------------------------------------------");
            place++;
        }








    }

}
