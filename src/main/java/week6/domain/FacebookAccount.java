package week6.domain;

public class FacebookAccount {
    protected String name;
    protected int numberOfFriends;
    FriendsLimit friendsLimit = new FriendsLimit();


    String substract;
    public FacebookAccount(String name, int numberOfFriends, FriendsLimit friendsLimit) {
        this.name = name;
        this.numberOfFriends = numberOfFriends;
        this.friendsLimit = friendsLimit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfFriends() {
        return numberOfFriends;
    }

    public void setNumberOfFriends(int numberOfFriends) {
        this.numberOfFriends = numberOfFriends;
    }

    public boolean addFriends(int friendsAdded){
        this.numberOfFriends += friendsAdded;
        return true;
    }

    public boolean removeFriends(int friendsRemoved){
        this.numberOfFriends -= friendsRemoved;
        return true;
    }

    public boolean deleteAllFriends(){
        this.numberOfFriends = 0;
        return true;
    }

    public int howManyFriendsCanYouAdd(FriendsLimit friendsLimit){
        return friendsLimit.getFriendsLimit() - this.numberOfFriends;
    }
}
