package week6.domain;

import java.util.*;

public class Calculator<arrOfString> {

    private String input;
    private String desiredUnitOfMeasure;

    public Calculator(String input, String desiredUnitOfMeasure) {
        this.input = input;
        this.desiredUnitOfMeasure = desiredUnitOfMeasure;
    }

    public static String calculate(String input, String desiredUnitOfMeasure) {


        /*Here I created three different containers in order to separate numbers from unitMeasures and operations.
        I have chosen this data structure because I wanted to use a FIFO structure (first in, first out), which would
        return me the numbers for instance exactly in the order in which I have introduced them.
        */
        Queue<Float> numbers = new LinkedList<>();
        Queue<String> unitMeasures = new LinkedList<>();
        Queue<String> operations = new LinkedList<>();

        String[] arrOfString = input.split(" ", input.length());


        // Here I separate the numbers from unit measures and operations. Each of them goes to its specific container.
        // I have made this separation based on the number of spaces between the operations.
        int appearance = 0;

        for (String a : arrOfString) {
            if (appearance == 0) {
                numbers.add(Float.parseFloat(a));
            } else if (appearance == 1) {
                unitMeasures.add(a);
            } else {
                operations.add(a);
            }
            appearance++;
            if (appearance > 2) appearance = 0;
        }



        HashMap<String, Integer> conversions = new HashMap<>();
        conversions.put("mm", 1);
        conversions.put("cm", 2);
        conversions.put("dm", 3);
        conversions.put("m", 4);
        conversions.put("km", 8);

        //Here I perform the operations and convert them to the lowest unit of measure (in this case, mm).

        double result = numbers.poll() * Math.pow(10, conversions.get(unitMeasures.poll()) - 1);

        //operation -> number -> unit of measure

        while (!operations.isEmpty()) {
            if (operations.peek().equals("+")) {
                result += numbers.poll() * Math.pow(10, conversions.get(unitMeasures.poll()) - 1);

            } else if (operations.peek().equals("-")) {
                result -= numbers.poll() * Math.pow(10, conversions.get(unitMeasures.poll()) - 1);
            } else {
                break;
            }
            operations.remove();
        }

        //casting the result to float for precision

        return ( (float) result * Math.pow(10, -conversions.get(desiredUnitOfMeasure) +1)) + " " + desiredUnitOfMeasure;

    }
}



