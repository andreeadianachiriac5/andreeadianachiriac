package week6;

import week6.domain.Calculator;
import week6.domain.FacebookAccount;
import week6.domain.FriendsLimit;

import java.util.*;

public class Main{

    /*
    For this homework I created more classes in order to have more possibilities of testing.
    My calculator receives an input like this : "10 cm + 1 m - 91 mm =" as first parameter and the desired unit
    of conversion as second parameter. Any space added or removed would ruin my program. I decided to create a public
    static method calculate inside the Calculator, in order to not be required the instantiation of an Object named
    calculator, as I found creating more calculators useless, tacking into account that two Calculators would not have
    any particularities. The algorithm on which is based my calculator was built in order to use different object
    containers and class methods (as Math, for instance). I made this homework as a revision for the exam which comes,
    and I tried to use as many concepts as we learned during the course. Maybe I overcomplicated the homework, but I wanted
    to use again almost everything we learned.

    I have also created another class named FacebookAccount without any direct relation to the calculator. I have
    just needed more instances to mock and test. :D
     */

    public static void main(String[] args) {
        System.out.println(Calculator.calculate("10 cm + 1 m - 91 mm =", "mm"));

        FriendsLimit friendsLimit = new FriendsLimit();
        FacebookAccount facebookAccount = new FacebookAccount("Crina", 4, friendsLimit);
        System.out.println(facebookAccount.howManyFriendsCanYouAdd(friendsLimit));
    }
}

