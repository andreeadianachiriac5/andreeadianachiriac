package week5.domain;

import java.util.List;
import java.util.Objects;

public class Person implements Comparable{
    protected String name;
    protected int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(List<String> tempArr) {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        final Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name);
    }

    @Override
    public int compareTo(Object o) {
        Person person = (Person) o;
        int comparedByName = name.compareTo(person.name);
        if (comparedByName == 0) {
            comparedByName = Integer.compare(age, person.getAge());
        }
        return comparedByName;
    }
}

