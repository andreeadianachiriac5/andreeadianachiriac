package week5.domain;

import week5.domain.Address;

public class Country extends Address {
    private String country;

    public Country(String city, int postalCode, String country) {
        super(city, postalCode);
        this.country = country;
    }


    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return country;
    }
}
