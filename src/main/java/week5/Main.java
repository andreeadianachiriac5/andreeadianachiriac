package week5;

import week5.domain.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class Main {
    public static void main(String[] args) {


        /* In the first part of my homework I will define some animals which I will sort using two comparators: one for
        the name,and one for their weight (in descending order). The purpose of this extra-exercise is to practice the
        way in which a comparator can be created without implementing Comparable. Also, for the first part of homework
        (which is applied on an extra class named Animal) I decided to simply print the set, as the purpose of it was
        only to use the comparators. The iteration with for(for (Person person: sortedPersons){}) I applied on the second
        part of homework because I wanted to apply both methods. */


        //Creating some random animals
        Animal animal1 = new Animal("Bear", 78);
        Animal animal2 = new Animal("Cow", 103);
        Animal animal3 = new Animal("Monkey", 40);
        Animal animal4 = new Animal("Zebra", 72);
        Animal animal5 = new Animal("Dog", 21);
        //This bear should not be added.
        Animal animal6 = new Animal("Bear", 78);
        Animal animal7 = new Animal("Llama", 40);


        //Creating a set with a comparator based on the name. As I have 2 bears, there will be 6 elements.
        Set<Animal> sortedByName = new TreeSet<>(new comparatorByName());
        sortedByName.add(animal1);
        sortedByName.add(animal2);
        sortedByName.add(animal3);
        sortedByName.add(animal4);
        sortedByName.add(animal5);
        sortedByName.add(animal6);
        sortedByName.add(animal7);
        System.out.println(sortedByName);
        System.out.println("This set has " + sortedByName.size() + " elements.");
        System.out.println("   ");


        /* Creating a set with a weight comparator. As I have 4 animals with the same weight (2 pairs of
        different weight. The length of the elements will be 5.*/

        Set<Animal> sortedByWeight = new TreeSet<>(new comparatorByWeight());
        sortedByWeight.add(animal1);
        sortedByWeight.add(animal2);
        sortedByWeight.add(animal3);
        sortedByWeight.add(animal4);
        sortedByWeight.add(animal5);
        sortedByWeight.add(animal6);
        sortedByWeight.add(animal7);
        System.out.println(sortedByWeight);
        System.out.println("This set has " + sortedByWeight.size() + " elements.");
        System.out.println("    ");

        //Creating some random persons
        Person person1 = new Person("Lia", 23);
        Person person2 = new Student("Daniela", 19);
        //Here I created another Daniela which is unemployed, but is still the same person.
        Person person3 = new Unemployed("Daniela", 19);
        Person person4 = new Hired("Mirela", 42);
        Person person5 = new Unemployed("Corina", 37);
        Person person6 = new Student("Mirela", 16);
        Person person7 = new Person("Ancuta", 42);


        /*Generating a container which will store elements using the alphabetical order and when it encounters two persons with the same name
        it will add the person which is younger. For this sorting I applied an override comparator in the Person class. */

        Set<Person> sortedPeople = new TreeSet<>();
        sortedPeople.add(person1);
        sortedPeople.add(person2);
        sortedPeople.add(person3);
        sortedPeople.add(person4);
        sortedPeople.add(person5);
        sortedPeople.add(person6);
        sortedPeople.add(person7);


        //Iterating through the list.
        for(Person person: sortedPeople){
            System.out.println("This person names " + person.getName() + " and is " + person.getAge() + " years old.");
        }

        System.out.println(" ");


        //Creating some addresses

        Address address1 = new Country("Berlin", 15230, "Germany");
        Address address2 = new Country("Paris", 7459, "France");
        Address address3 = new Country("Moscow", 1240, "Russia");
        Address address4 = new Country("Tokyo", 7540, "Japan");
        Address address5 = new Country("Prague", 24932, "Czech Republic");
        Address address6 = new Country("Peking",28744, "China");
        Address address7 = new Country("Minsk", 23948, "Belarus");
        Address address8 = new Country("Bucharest", 93485, "Romania");
        Address address9 = new Country("Lviv", 10230, "Ukraine");


        //Creating some particular hobbies

        Hobby hobby1 = new Hobby("Carting", 3, List.of(address3, address9, address2, address8));
        Hobby hobby2 = new Hobby("Driving", 2, List.of(address1, address4));
        Hobby hobby3 = new Hobby("Swimming", 6, List.of(address5, address9, address3));
        Hobby hobby4 = new Hobby("Golf", 1, List.of(address6, address7));
        Hobby hobby5 = new Hobby("Ridding", 3, List.of(address1, address7, address6, address9));
        Hobby hobby6 = new Hobby("Hunting", 2, List.of(address7));
        Hobby hobby7 = new Hobby("Driving", 4, List.of(address1, address7, address5));

        //Mapping the persons with their hobbies

        HashMap<Person, List<Hobby>> passionsMap = new HashMap<>();
        passionsMap.put(person1, List.of(hobby1, hobby6));
        passionsMap.put(person2, List.of(hobby3, hobby4, hobby6));
        passionsMap.put(person3, List.of(hobby7, hobby3, hobby5, hobby2, hobby6));
        passionsMap.put(person4, List.of(hobby4, hobby5));
        passionsMap.put(person5, List.of(hobby7));
        passionsMap.put(person6, List.of(hobby6, hobby2));


        /*Here I made some changes into the toString() method in order to display (as was requested) only the name of the person and
        its according hobbies along with the places where these can be practised.
        ex: Lia : [Carting -> [Russia, Ukraine, France, Romania], Hunting -> [Belarus]]
        */
        for (Person person: passionsMap.keySet()){
            System.out.println(person.getName() + " : " + passionsMap.get(person));
        }

    }
}
