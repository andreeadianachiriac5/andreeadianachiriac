package week4.domain;

abstract class Volkswagen extends Car{
    public Volkswagen(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }

    //Atributs which are specific to all Volkswagen cars

    protected final int fuelTankSize = 1000;
    protected final String fuelType = "PETROL";
    protected static int gears;
    protected static int tireSize = 14;

    //Accessing proprieties of this particular model without permission change them outside a method (setter).


    public int getFuelTankSize() {
        return fuelTankSize;
    }

    public String getFuelType() {
        return fuelType;
    }

    public static int getGears() {
        return gears;
    }

    public static int getTireSize() {
        return tireSize;
    }

    @Override
    public String toString() {
        return "Volkswagen{" +
                "availableFuel=" + availableFuel +
                ", fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                "} " + super.toString();
    }
}
