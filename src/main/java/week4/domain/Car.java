package week4.domain;


public abstract class Car extends Vehicle{

    private String chassisNumber;
    protected static int tireSize = 14;
    protected static int gears;

    public Car(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }


    // Proprieties which can be configured by the owner
    public void setAvailableFuel(float addFuel) {
        this.availableFuel = addFuel;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    //Generating the gears of the car and changing the fuel consumption acording this.
    public void shiftGear(int gears){
        this.gears = gears;
        if(gears <= 0){
            stop();
        }else if (gears>this.gears) {
            this.consumptionPer100Km -= (gears - this.gears) * this.consumptionPer100Km * gearProcent;
        }else {
            this.consumptionPer100Km += (gears - this.gears) * this.consumptionPer100Km * gearProcent;
        }
    }

    //Generating the number of tires of the car and changing the fuel consumption acording this.

    public void shiftTires(int tireSize){
        this.tireSize = tireSize;

        if(tireSize > this.tireSize){
            this.consumptionPer100Km += (tireSize - this.tireSize) * this.consumptionPer100Km * tireProcent;
        }else {
            this.consumptionPer100Km -= (tireSize - this.tireSize) * this.consumptionPer100Km * tireProcent;
        }
    }


    //See the proprieties


    public static int getTireSize() {
        return tireSize;
    }

    public static int getGears() {
        return gears;
    }

    @Override
    public String toString() {
        return "Car{" +
                "chassisNumber='" + chassisNumber + '\'' +
                ", availableFuel=" + availableFuel +
                "} " + super.toString();
    }
}
