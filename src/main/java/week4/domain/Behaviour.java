package week4.domain;

public interface Behaviour {
    void start();
    void stop();
    void drive(double distance);
}
