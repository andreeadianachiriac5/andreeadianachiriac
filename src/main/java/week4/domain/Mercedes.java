package week4.domain;

abstract class Mercedes extends Car{


    //Atributs which are specific to all Mercedes cars

    protected final int fuelTankSize = 1500;
    protected final String fuelType = "DIESEL";
    protected static int gears;
    protected static int tireSize = 14;

    public Mercedes(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }


    //Accessing proprieties of this particular model without permission change them outside a method (setter).


    public int getFuelTankSize() {
        return fuelTankSize;
    }

    public String getFuelType() {
        return fuelType;
    }

    public static int getGears() {
        return gears;
    }

    public static int getTireSize() {
        return tireSize;
    }

    @Override
    public String toString() {
        return "Mercedes{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", availableFuel=" + availableFuel +
                "} " + super.toString();
    }
}
