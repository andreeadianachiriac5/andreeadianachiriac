package week4.domain;

public class SKlasse extends Mercedes{


    public SKlasse(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }

    @Override
    public void shiftGear(int gears) {
        super.consumptionPer100Km = 6.2;
        super.gearProcent = 2.4;
        super.shiftGear(gears);
    }

    @Override
    public void shiftTires(int tireSize) {
        super.consumptionPer100Km = 6.2;
        super.tireProcent = 3.1;
        super.shiftTires(tireSize);
    }

    @Override
    public String toString() {
        return "SKlasse{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", availableFuel=" + availableFuel +
                "} " + super.toString();
    }
}
