package week4.domain;

public class VWPassat extends Volkswagen{


    public VWPassat(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }

    @Override
    public void shiftGear(int gears) {
        super.consumptionPer100Km = 7.8;
        super.gearProcent = 1.2;
        super.shiftGear(gears);
    }

    @Override
    public void shiftTires(int tireSize) {
        super.consumptionPer100Km = 7.8;
        super.tireProcent = 2.7;
        super.shiftTires(tireSize);
    }

    @Override
    public String toString() {
        return "VWPassat{" +
                "availableFuel=" + availableFuel +
                ", fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                "} " + super.toString();
    }
}
