package week4.domain;

public class CKlasse extends Mercedes{


    public CKlasse(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }

    @Override
    public void shiftGear(int gears) {
        super.consumptionPer100Km = 5.7;
        super.gearProcent = 1.1;
        super.shiftGear(gears);
    }

    @Override
    public void shiftTires(int tireSize) {
        super.consumptionPer100Km = 5.7;
        super.tireProcent = 1.8;
        super.shiftTires(tireSize);
    }

    @Override
    public String toString() {
        return "CKlasse{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", availableFuel=" + availableFuel +
                "} " + super.toString();
    }
}
