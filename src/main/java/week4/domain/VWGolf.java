package week4.domain;

public class VWGolf extends Volkswagen{


    public VWGolf(double availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber);
    }

    @Override
    public void shiftGear(int gears) {
        super.consumptionPer100Km = 6.3;
        super.gearProcent = 3.4;
        super.shiftGear(gears);
    }

    @Override
    public void shiftTires(int tireSize) {
        super.consumptionPer100Km = 6.3;
        super.tireProcent = 2.3;
        super.shiftTires(tireSize);
    }

    @Override
    public String toString() {
        return "VWGolf{" +
                "availableFuel=" + availableFuel +
                ", fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                "} " + super.toString();
    }
}
