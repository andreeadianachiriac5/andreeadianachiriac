package week4.domain;

public abstract class Vehicle implements Behaviour{
    protected static double averageFuelConsumption;
    protected double availableFuel;
    protected static double consumptionPer100Km;
    private String chassisNumber;
    protected static double tireProcent = 1;
    protected static double gearProcent = 1;

    //Constructor object based on fuel and chassis number
    public Vehicle(double availableFuel, String chassisNumber) {
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
    }

    //Accessing the proprieties of the car without modifying them


    public static double getAverageFuelConsumption() {
        return averageFuelConsumption;
    }

    public double getAvailableFuel() {
        return this.availableFuel;
    }

    public static double getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public static double getTireProcent() {
        return tireProcent;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    @Override
    public void start() {
        this.averageFuelConsumption = 0;
    }

    @Override
    public void stop() {
        System.out.println("You reached the destination. You consumed " + this.averageFuelConsumption);
    }

    @Override
    public void drive(double distance) {
        if (distance * this.consumptionPer100Km <= 0){
            System.out.println("You do not have enough fuel");
        }else{
            this.availableFuel -= distance * this.consumptionPer100Km;
            averageFuelConsumption += distance * this.consumptionPer100Km;
        }

    }


    //Read

    @Override
    public String toString() {
        return "Vehicle{" +
                "availableFuel=" + availableFuel +
                ", chassisNumber='" + chassisNumber + '\'' +
                '}';
    }
}
