package week4;


import week4.domain.*;

public class Main {
    public static void main(String[] args) {
        //This should not compile
        //Car car = new Car();

        //This should also not compile
        //Car car = new Mercedes();
        Car diu = new VWPassat(57, "AG40DIU");
        System.out.println(diu);
        diu.start();
        diu.shiftGear(1);
        diu.drive(0.32);
        System.out.println(diu.getAvailableFuel());
        diu.shiftGear(2);
        diu.drive(0.02);
        diu.shiftGear(3);
        System.out.println("Gears " + diu.getGears());
        diu.drive(0.5);
        diu.shiftGear(4);
        System.out.println(diu.getAvailableFuel());
        System.out.println("Ai consumat + " +diu.getAverageFuelConsumption());
        System.out.println(diu);
        diu.start();
        System.out.println(diu.getAverageFuelConsumption());
        System.out.println("----------------------------------------------");

        Vehicle cip = new SKlasse(100, "B26CIP");
        System.out.println(cip);
        cip.start();
        cip.drive(10);
        cip.stop();
        System.out.println(cip);
        cip.start();
        Car cipri = (Car) cip;
        cipri.drive(4);
        cipri.setAvailableFuel(100);
        System.out.println("You have " + cipri.getAvailableFuel() + " fuel");
        cipri.shiftGear(0);
    }
}
