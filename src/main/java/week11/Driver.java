package week11;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Driver {
    public Connection connect_to_db(String dbname, String user, String pass){
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + dbname, user, pass);
            if (conn != null){
                System.out.println("Connection established");
            }else {
                System.out.println("Connection failed");
            }


        }catch (Exception e){
            System.out.println(e);
        }
        return conn;
    }

    public void createTable(Connection connection, String table_name){
        Statement statement;
        try{
            String query = " create table " + table_name + " (id int primary key, type varchar(32), bed_type varchar(32), max_guests int, description varchar(512))";
            statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println(" Table Created ");
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public void insert_row(Connection connection, String table_name, int id, String type, String bed_type, int max_guests, String description){
        Statement statement;
        try {
            String query = String.format(" insert into %s( id, type, bed_type, max_guests, description) values('%s', '%s', '%s', '%s', '%s');", table_name, id, type, bed_type, max_guests, description);
            statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println("Row inserted");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
