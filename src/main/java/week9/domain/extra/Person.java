package week9.domain.extra;

import java.time.LocalDate;

public class Person extends Exception{
    private String firstName;
    private String secondName;
    private LocalDate birthdate;
    private String occupation;


    public Person(String firstName, String secondName, String birthdate, String occupation){
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthdate = LocalDate.parse(birthdate);
        this.occupation = occupation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthdate=" + birthdate +
                ", occupation='" + occupation + '\'' +
                '}';
    }
}
