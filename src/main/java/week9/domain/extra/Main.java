package week9.domain.extra;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        //1) Write 5 different instances of that object.
        Person person1 = new Person("Diana", "Chiriac", "1999-07-05", "Student");
        Person person2 = new Person("Mirela", "Popa", "1970-01-25", "TV Star");
        Person person3 = new Person("Ancuta", "Popescu", "1987-03-15", "Teacher");
        Person person4 = new Person("Lavinia", "Mihai", "1999-09-05", "Student");
        Person person5 = new Person("Mircea", "Dragomir", "1970-03-14", "Teacher");

        List<Person> personList = List.of(person1, person2, person3, person4, person5);


        //2) Find the elements containing the letter "a" that start with "M" and print them out.
        Set<Person> filterByName = personList.
                stream().
                filter(
                el -> {
                    return el.getFirstName().startsWith("M") && el.getFirstName().contains("a");
                }
        ).collect(Collectors.toSet());

        //3) Find the "min" using a custom comparing criteria of choice.
        Optional<Person> minPerson = personList.stream().min(
                new Comparator<Person>() {
                    @Override
                    public int compare(Person o1, Person o2) {
                        return o1.getBirthdate().compareTo(o2.getBirthdate());
                    }
                }
        );
        System.out.println(minPerson);


        //4) Generate 5 random Strings and add them to a Set. Find the "max" (while explaining as Javadoc how comparing Strings works)
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        Set<String> randomStrings = new HashSet<>();
        for (int i = 0; i < 4; i++){
            String generatedString = random.ints(leftLimit, rightLimit + 1)
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            randomStrings.add(generatedString);
        }

        Optional<String> maximString = randomStrings.stream().max(
                new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return -o1.compareTo(o2);
                    }
                }
        );


        //5) Generate a random number of Integers and then count them. "Map" the exponential to the numbers and then print them out.
        new Random()
                .ints(new Random().nextInt(10))
                .forEach(System.out::println);
        long counts = new Random().
                ints(new Random().nextInt(10))
                .count();
        System.out.println(counts);


        //6) Create a map of "n" (K,V) elements and print "how many" elements have value over 10 (the key is of your choice)
        HashMap<String, Integer> map = new HashMap<>(){};
        map.put("Diu", 11);
        map.put("Cipri", 9);
        map.put("Liviu", 14);
        map.put("Andrei", 8);
        map.put("Marius", 6);
        map.put("Larisa", 4);
        map.put("Amelia", 23);

        map.keySet()
                .stream()
                .filter(
                        el -> {
                            return map.get(el) >= 10;
                        }
                )
                .forEach(System.out::println);

        //7) Sort the above Set<String> (used for max) in reverse order

        randomStrings.stream()
                .sorted(
                        new Comparator<String>() {
                            @Override
                            public int compare(String o1, String o2) {
                                return -o1.compareTo(o2);
                            }
                        }
                )
                .forEach(System.out::println);

        //8) Sort the above List of custom objects (used for filter) in an order you consider

        personList.stream()
                .sorted(
                        new Comparator<Person>() {
                            @Override
                            public int compare(Person o1, Person o2) {
                                return o1.getSecondName().compareTo(o2.getSecondName());
                            }
                        }
                )
                .forEach(System.out::println);



        //9) Check if any of your instances "match" a condition based on an Integer field (of your choice). What does it return ? Print it out.
        boolean containsVowel = personList.stream()
                .anyMatch(
                        el -> {
                            return el.getSecondName().contains("a");
                        }
                );
        System.out.println(containsVowel);

        //10) What does Optional represent ? Explain through an example on your custom object
        //
        //-> Wrap an existing instance
        //
        //-> Wrap a null
        //
        //-> Check value using ifPresent or isPresent


        Optional<String> empty = Optional.empty();

        String name = "Diana";
        Optional<String> obj = Optional.of(name);
        System.out.println(obj);
        System.out.println(obj.isPresent());
        System.out.println(obj.isEmpty());

        String emptyString = null;
        System.out.println(Optional.ofNullable(emptyString).orElse("Diu"));


        //11) Fastest way to find the shortest String in a List (take the 5 random Strings generated above and find the shortest one).
        List<String> randomNames = List.of("Ruxi", "Alina","D", "Mihaela", "Georgi");
        String min = randomNames.stream()
                        .min(
                                new Comparator<String>() {
                                    @Override
                                    public int compare(String o1, String o2) {
                                        return o1.length() - o2.length();
                                    }
                                }
                        )
                                .get();
        System.out.println(min);





    }
}
