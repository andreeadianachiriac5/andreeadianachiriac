package week9.homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ReadFromFile {

    private static String line;

    public static Set<Person> read(String path) throws IOException {

        Set<Person> personSet = new HashSet<>();
        BufferedReader reader = new BufferedReader(new FileReader(path));
        while ((line = reader.readLine()) != null){
            personSet.add(new Person(line.split(",")));
        }

        return personSet;

    }


}
