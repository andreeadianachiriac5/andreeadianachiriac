package week9.homework;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Aplication {

    private String inputFilename;
    private int targetMonth;
    private String outputFilename;

    public Aplication(String inputFilename, int targetMonth, String outputFilename) {
        this.inputFilename = inputFilename;
        this.targetMonth = targetMonth;
        this.outputFilename = outputFilename;
    }

    public String getInputFilename() {
        return inputFilename;
    }

    public void setInputFilename(String inputFilename) {
        this.inputFilename = inputFilename;
    }

    public int getTargetMonth() {
        return targetMonth;
    }

    public void setTargetMonth(int targetMonth) {
        this.targetMonth = targetMonth;
    }

    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(String outputFilename) {
        this.outputFilename = outputFilename;
    }

    public void write() throws IOException {


        Set<Person> personSet = ReadFromFile.read(this.inputFilename).stream().filter(
                el -> {
                    return el.getBirthdate().getMonth().getValue() == targetMonth;
                }
        ).collect(Collectors.toSet());

        WriteToFile.writeFile(outputFilename, personSet);
    }
}
