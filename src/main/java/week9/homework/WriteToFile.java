package week9.homework;

import java.io.*;
import java.util.Set;

public class WriteToFile {
    public static void writeFile(String path, Set<Person> personSet) throws IOException{
        File fOut = new File(path);
        FileOutputStream fos = new FileOutputStream(fOut);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (Person person: personSet){
            bw.write(person.getFirstName() + " " + person.getLastName());
            bw.newLine();
        }
        bw.close();
    }
}
