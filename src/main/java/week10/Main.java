package week10;


import week10.domain.*;

import java.util.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        boolean open = true;
        List<FestivalGate> gates = Arrays.asList(new FestivalGate(), new FestivalGate(), new FestivalGate());
        FestivalStatisticsThread festivalStatisticsThread = new FestivalStatisticsThread(gates.get(0));
        Queue<Person> people = new ArrayDeque<>();
        long total = 0;

        while (open){
            for (int i = 0; i < new Random().nextInt(); i++){
                people.add(new Person());
            }

            for (Person person: people){
                FestivalGate actualGate = gates.get(new Random().nextInt(gates.size()));
                FestivalAttendeeThread attendeeThread = new FestivalAttendeeThread(person.getTicketType(), actualGate);
                attendeeThread.run(actualGate);
                total++;
                if (festivalStatisticsThread.getEntries().containsKey(person.getTicketType())){
                    festivalStatisticsThread.getEntries().put(person.getTicketType(), festivalStatisticsThread.getEntries().get(person.getTicketType()) + 1);
                }else {
                    festivalStatisticsThread.getEntries().put(person.getTicketType(), 1);
                }



            }
            Thread.sleep(5000);
            System.out.println("Let me check...");
            System.out.print(total + " people were checked. Here are the places up to now: ");
            System.out.println(festivalStatisticsThread.getEntries());



        }

    }
}
