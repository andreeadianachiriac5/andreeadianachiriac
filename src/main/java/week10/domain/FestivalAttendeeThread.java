package week10.domain;

public class FestivalAttendeeThread implements Runnable{

    private TicketType ticketType;
    private FestivalGate gate;

    public FestivalAttendeeThread(TicketType ticketType, FestivalGate gate) {
        this.ticketType = ticketType;
        this.gate = gate;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }

    public FestivalGate getGate() {
        return gate;
    }

    public void setGate(FestivalGate gate) {
        this.gate = gate;
    }


    public void run(FestivalGate actualGate) {
    }


    @Override
    public void run() {

    }
}
