package week10.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Person {

    private List<TicketType> availableTickets = List.of(TicketType.values());
    private TicketType ticketType = availableTickets.get(
            new Random().nextInt(availableTickets.size()));

    public TicketType getTicketType(){
        return ticketType;
    }

    @Override
    public String toString() {
        return "Person{" +
                "availableTickets=" + availableTickets +
                ", ticketType=" + ticketType +
                '}';
    }
}
