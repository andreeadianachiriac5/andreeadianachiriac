package week10.domain;

import java.util.List;
import java.util.Random;

public enum TicketType {
    FULL,
    FULL_VIP,
    FREE_PAS,
    ONE_DAY,
    ONE_DAY_VIP,
}
