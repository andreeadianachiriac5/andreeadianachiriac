package week10.domain;

import java.util.HashMap;
import java.util.Map;

public class FestivalStatisticsThread implements Runnable{

    private FestivalGate gate;
    Map<TicketType, Integer> entries = new HashMap<>();

    public FestivalStatisticsThread(FestivalGate gate){
        this.gate = gate;
    }

    public FestivalGate getGate() {
        return gate;
    }

    public void setGate(FestivalGate gate) {
        this.gate = gate;
    }

    public Map<TicketType, Integer> getEntries() {
        return entries;
    }

    public void setEntries(Map<TicketType, Integer> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "FestivalStatisticsThread{" +
                "gate=" + gate +
                ", entries=" + entries +
                '}';
    }

    @Override
    public void run() {
        

    }
}
